#include <iostream>
#include <string>
#include <iostream>
#include <string>
#include <ctime>
#include "tree.h"
#include <ctime>
using namespace std;
int main()
{
	int n,y,del;
	Tree<int> mytree;
	srand(100);
	for (int i = 0; i < 50; i++)
	{
		y = rand() % 100 + 1;
		mytree.insert(y);
	}
	int t;
	t=mytree.getheight();
	cout << "\nHeight of Trees is: " << t << endl; 
	mytree.Array(50);
	t = mytree.getheight();
	cout << "\nNew height of Trees  after balance is: " << t << endl;
	cout<< "Insert number of Random numbers:";
	
	cin >> n;
	for (int i = 0; i < n; i++)
	{
		y = rand() % 100 + 1;
		cout << i+1<< ". Insert " << y <<" to the Trees"<< endl;
		mytree.insert(y);
	}
	cout << "What you want to find out:";
	int fo;
	cin >> fo;
	if (mytree.search(fo) == true)
		cout << fo << " is in Trees." << endl;
	else
		cout << fo << " isn't in Trees." << endl;

	cout << "How many deletion operations should be performed :";
	cin >> del;
	for (int i = 0; i < del; i++)
	{
		y = rand() % 100 + 1;
		cout << i+1 << ". Delete " << y << " from Trees" << endl;
		mytree.deletex(y);
	}
}

/*


BINARY TREE SIMULATION


* Insert 500 Random numbers into a binary tree – output the height of the tree.
* Balance the tree – output the new height of the tree.
* Add an interface which allows the user to decide how many ‘insertion’ operations should be done.
* Allow the user to search to find out if a particular node is in the tree.
* Allow the user to decide how many deletion operations should be performed (i.e. generate a random number, and if that number is in the tree, delete it).
*/
