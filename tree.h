#include <queue>
#include <stack>
#include <algorithm>
using namespace std;


#ifndef Binary_Search_Tree
#define Binary_Search_Tree

template<class T> class Tree;

template<class T>
class Node {
public:
	Node() { left = right = NULL; }
	Node(const T& el, Node *l = 0, Node *r = 0) {
		key = el; left = l; right = r;
	}
	T key;
	Node *left, *right;
};

template<class T>
class Tree {
public:
	Tree() { root = 0; }
	~Tree() { clear(); }
	void clear() { clear(root); root = 0; }
	bool isEmpty() { return root == 0; }
	void inorder() { inorder(root); }
	void insert(const T& el);
	void visit(Node<T> *p);
	int getheight(Node<T> *p);
	int getheight() { return getheight(root); }
	void _push(Node<T> *p);
	queue<int> box;
	void Array(int n);
	void balance(T data[], int f, int l);
	bool search(const T& el) const;
	void deleteNode(Node<T> *& node, Node<T> *& perent);
	void deletex(int el);
	void printroot();
	int height = 0;
	Node<T> *root;
	Node<T> *tmp;
protected:
	void clear(Node<T> *p);
	void inorder(Node<T> *p);
};

template<class T>
void Tree<T>::clear(Node<T> *p) {
	if (p != 0) {
		clear(p->left);
		clear(p->right);
		delete p;
	}
}

template<class T>
void Tree<T>::inorder(Node<T> *p) {
	if (p != 0) {
		inorder(p->left);
		visit(p);
		inorder(p->right);
	}
}

template<class T>
void Tree<T>::insert(const T &el) {
	Node<T> *p = root, *prev = 0;
	while (p != 0) {
		prev = p;
		if (p->key < el)
			p = p->right;
		else
			p = p->left;
	}
	if (root == 0)
		root = new Node<T>(el);
	else if (prev->key<el)
		prev->right = new Node<T>(el);
	else
		prev->left = new Node<T>(el);
}

template<class T>
void Tree<T>::visit(Node<T> *p)
{
	cout << p->key << endl;

}

template<class T>
bool Tree<T>::search(const T& el) const
{
	Node<T> *p = root;
	while (p != 0) {
		if (el == p->key) {
			return true;
		}
		else if (el < p->key) {
			p = p->left;
		}
		else {
			p = p->right;
		}
	}
	return false;
}

template<class T>
void Tree<T>::_push(Node<T> *p) {
	if (p != 0){
		_push(p->left);
		box.push(p->key);
		_push(p->right);
	}
}

template<class T>
void Tree<T>::Array(int n) {
	_push(root);
	int *arry = new int[n];
	for (int i = 0; i < n; i++) {
		arry[i] = box.front();
		box.pop();
	}
	clear();
	height = 0;
	balance(arry, 0, n - 1);
}

template<class T>
int Tree<T>::getheight(Node<T> *p) {
	if (p == 0) {
		return 0;
	}
	else {
		return max(getheight(p->left), getheight(p->right)) + 1;
	}
}

template<class T>
void Tree<T>::balance(T data[], int f, int l) {
	if (f <= l){
		int middle = (f + l) / 2;
		insert(data[middle]);
		balance(data, f, middle - 1);
		balance(data, middle + 1, l);
	}
}

template<class T>
void Tree<T>::deletex(int el) {
	Node<T> *p = root, *perent = p;
	while (p != 0) {
		if (el == p->key) {
			deleteNode(p, perent);
			break;
		}
		else if (el < p->key) {
			perent = p;
			p = p->left;
		}
		else {
			perent = p;
			p = p->right;
		}
	}
}

template<class T>
void Tree<T>::deleteNode(Node<T> *&node, Node<T> *&perent) {
	Node<T> *prev, *tmp = node, *before = perent;
	if (node->right == 0) {
		if (before->left == node) {
			if (node->left == 0) {
				if (before->key > node->key) {
					before->left = 0;
				}
				else {
					before->right = 0;
				}
			}
			else {
				node = node->left;
				before->left = node;
			}
		}
		else {
			if (node->left == 0) {
				if (before->key > node->key) {
					before->left = 0;
				}
				else {
					before->right = 0;
				}
			}
			else {
				node = node->left;
				before->right = node;
			}
		}
	}
	else if (node->left == 0) {
		if (before->right == node) {
			if (node->right == 0) {
				if (before->key > node->key) {
					before->left = 0;
				}
				else {
					before->right = 0;
				}
			}
			else {
				node = node->right;
				before->left = node;
			}
		}
		else {
			if (node->right == 0) {
				if (before->key > node->key) {
					before->left = 0;
				}
				else {
					before->right = 0;
				}
			}
			else {
				node = node->right;
				before->right = node;
			}
		}
	}
	else {
		tmp = node->left;
		prev = node;
		while (tmp->right != 0) {
			prev = tmp;
			tmp = tmp->right;
		}
		node->key = tmp->key;
		if (prev == node) {
			prev->left = tmp->left;
		}
		else {
			prev->right = tmp->left;
		}
	}
	delete tmp;
}
#endif // Binary_Search_Tree